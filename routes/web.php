<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'PagesController@home')->name('home');
Route::get('about', 'PagesController@about');
Route::get('wellness', 'PagesController@wellness');
Route::get('services', 'PagesController@services');
Route::get('products', 'PagesController@products');
Route::get('product', 'PagesController@product');
Route::get('calendar', 'PagesController@calendar');
Route::get('contact-me', 'PagesController@contact_me');
Route::get('policies', 'PagesController@policies');
Route::get('testimonials', 'PagesController@testimonials')->name('testimonials');

Route::resource('cart', 'CartController');

Route::post('contact-me', 'FormsController@contact_me');

Route::get('checkout', 'AuthorizeController@index');
Route::post('checkout', 'AuthorizeController@chargeCreditCard');

Auth::routes();

Route::get('/home', 'HomeController@index');

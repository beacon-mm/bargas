<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     *  Homepage
     *
     *
     */

    public function home()
    {
        $block_how = json_decode(file_get_contents('https://closerdesign.net/api/static-content/1'));
        $block_what = json_decode(file_get_contents('https://closerdesign.net/api/static-content/2'));

        return view('pages.home', compact('block_how', 'block_what'));
    }

    /**
     *  About
     *
     *
     */

    public function about()
    {
        return view('pages.about');
    }

    /**
     *  Wellness
     *
     *
     */

    public function wellness()
    {
        return view('pages.wellness');
    }

    /**
     *  Services
     *
     *
     */

    public function services()
    {
        $services = "https://closerdesign.net/api/posts-by-category/22";
        $services = file_get_contents($services);
        $services = json_decode($services, true);

        return view('pages.services', compact('services'));
    }

    /**
     *  Products
     *
     *
     */

    public function products()
    {
        return view('pages.products');
    }

    /**
     *  Products
     *
     *
     */

    public function product()
    {
        return view('pages.product');
    }



    /**
     *  Calendar
     *
     *
     */

    public function calendar()
    {
        try
        {
            $client = new Client();

            $response = $client->get('http://closerdesign.net/api/posts-by-category/72');

            $events = json_decode( $response->getBody() );

            return view('pages.calendar', compact('events'));
        }

        catch ( \Exception $e )
        {
            return $e->getMessage();
        }
    }

    /**
     *  Contact Me
     *
     *
     */

    public function contact_me()
    {
        return view('pages.contact-me');
    }

    /**
     *  Policies
     *
     *
     */

    public function policies()
    {
        return view('pages.policies');
    }

    /**
     *  Testimonials
     */

    public function testimonials()
    {
        return view('pages.testimonials');
    }
}

<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FormsController extends Controller
{
    /**
     *  Contact Form
     *
     *
     */

    public function contact_me(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'phone'      => 'required',
            'comments'    => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

        $client = new Client();

        try {

            $response = $client->post('https://closerdesign.net/api/lead/42', [
                'form_params' => $request->toArray()
            ]);

        } catch ( \Exception $e )
        {
            return $e;
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Thank you! We\'ll be in contact soon.'
        ]);

        return back();
    }
}

<!DOCTYPE html>
<!--[if IE 8]>    <html class="ie8" > <![endif]-->
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if IE 11]>    <html class="ie11" > <![endif]-->
<!--[if (gt IE 11)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <title>Bargas Wellness</title>
    <meta name="keywords" content="">
    <meta name="description" content="Our goal at Bargas Wellness is to make people feel better and to function at their highest potential. We hope in doing that, we help people live longer, more fulfilled lives. We accomplish this through education and empowering the patient to be the leader of their therapeutic team.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    {{--<link rel="icon" type="image/png" href="image/favicon.png">--}}
    <!--Bootstrap-->
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css">
    <!--font Awesome-->
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!--slider-->
    <link href="/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="/css/animate.css" rel="stylesheet" type="text/css">
    <!--main file-->
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/switcher.css">
    <!-- Default Color-->
    <link href="/css/theme-colors/default-color.css" rel="stylesheet" id="color" type="text/css">
    <!--Responsive file-->
    <link href="/css/responsive.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=425960334187885&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

@if ( Session::has('message') )
<div class="alert alert-{{ Session::get('message')['type'] }}">
    {{ Session::get('message')['message'] }}
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div id="boxed">
    <div class="inner_box">
        <!--Start Top Bar-->
        <div class="top_bar hidden-xs">
            <div class="container-fluid">
                <div class="bar_inner">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <ul>
                                <li><i class="fa fa-phone"></i> CALL US <strong>405-607-4445</strong></li>
                                <li><i class="fa fa-envelope"></i> hello@bargaswellness.com</li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-4 text-right">
                            <a href="#" class="button pink"> <img src="image/user.png" alt=""/> Get Appointment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Top Bar-->
        <!--Start Header-->
        <header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="logo">
                            <a href="{{ URL::to('') }}">
                                <img src="/image/bargas-logo.png" alt="Bargas Wellness"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-12 text-right">
                        <nav>
                            <ul id="nav">
                                <li><a href="{{ URL::to('') }}">HOME</a></li>
                                <li><a href="{{ action('PagesController@about') }}">ABOUT DR. ROYCE BARGAS</a></li>
                                <li><a href="{{ action('PagesController@wellness') }}">WHAT IS WELLNESS?</a></li>
                                <li><a href="{{ action('PagesController@services') }}">SERVICES</a></li>
                                <li><a href="{{ action('PagesController@products') }}">PRODUCTS</a></li>
                                <li><a href="{{ action('PagesController@calendar') }}">CALENDAR</a></li>
                                <li><a href="{{ route('testimonials') }}">TESTIMONIALS</a></li>
                                <li><a href="{{ action('PagesController@contact_me') }}">CONTACT ME</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!--End Header-->
        <!--Responsive Nav-->
        <div class="responsive_button">
            <p>Home</p>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="responsive_nav collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('') }}">HOME</a></li>
                <li><a href="{{ action('PagesController@about') }}">ABOUT DR. ROYCE BARGAS</a></li>
                <li><a href="{{ action('PagesController@wellness') }}">WHAT IS WELLNESS?</a></li>
                <li><a href="{{ action('PagesController@services') }}">SERVICES</a></li>
                <li><a href="{{ action('PagesController@products') }}">PRODUCTS</a></li>
                <li><a href="{{ action('PagesController@calendar') }}">CALENDAR</a></li>
                <li><a href="{{ route('testimonials') }}">TESTIMONIALS</a></li>
                <li><a href="{{ action('PagesController@contact_me') }}">CONTACT ME</a></li>
            </ul>
        </div>
        <!--Responsive Nav-->

        @yield('content')

        <section id="abim">
            <div class="container">
                <div class="col-md-4">
                    <img class="img-responsive" src="/image/logo-and-text.svg" alt="American Board of Internal Medicine">
                </div>
                <div class="col-md-8 col-md-offset-0 col-xs-10 col-xs-offset-1">
                    <a href="https://www.abim.org/verify-physician/Bargas-Royce-ynaD65tbfmU=.aspx" target="_blank">
                        <img src="/image/AbimBadge1-image.jpg" alt="View my Board Certification Status" class="img-responsive abim-badge">
                    </a>
                </div>
            </div>
        </section>

    <!-- Begin Mailchimp Signup Form -->
        <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%; padding-bottom: 100px;}
            /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
               We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
            <form action="https://bargaswellness.us20.list-manage.com/subscribe/post?u=e9a5bf9ec1f1d9f98173c7f4e&amp;id=91c387e7cb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <label for="mce-EMAIL">Subscribe</label>
                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e9a5bf9ec1f1d9f98173c7f4e_91c387e7cb" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
        </div>

        <!--End mc_embed_signup-->

        <!--Start Footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="footer_detail">
                            <p>Contact Bargas Wellness!</p>
                            <hr />
                            <span>3839 S Boulevard St, Suite 100<br />
                                Edmond OK, 73013</span>
                            <ul>
                                <li><i class="fa fa-phone"></i> Call Us 405-607-4445</li>
                                <li><i class="fa fa-envelope"></i> hello@bargaswellness.com</li>
                            </ul>
                            <a href="{{ URL::to('') }}#request-appointment" class="button"> <img src="image/user.png" alt=""/> Request Appointment</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="footer_widget">
                            <div class="fb-page" data-href="https://www.facebook.com/bargaswellness" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/bargaswellness" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bargaswellness">Bargas Wellness</a></blockquote></div>
                        </div>
                        <div class="footer_widget">
                            <h4>Social Links</h4>
                            <div class="social">
                                <a
                                        href="https://www.facebook.com/bargaswellness"
                                        target="_blank"
                                >
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a
                                        href="https://www.instagram.com/bargaswellness/"
                                        target="_blank"
                                >
                                    <i class="fa fa-instagram"></i>
                                </a>
                                <a
                                        href="https://www.youtube.com/channel/UCQfu2a9aw6LtGP89x7XdV3Q"
                                        target="_blank"
                                >
                                    <i class="fa fa-youtube"></i>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_bottom">
                    <p>All rights reserved &copy; Bargas Wellness. {{ date('Y') }}. <a href="{{ action('PagesController@policies') }}">| Privacy Policy </a>
                        <a href="{{ action('PagesController@policies') }}">| Refund / Return Policy</a></p>
                </div>
            </div>
        </footer>
        <!--End Footer-->
    </div>
</div>
<a href="#0" class="cd-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

<!-- jQuery -->
<script src="/js/jquery.js"></script>
<!-- Bootstrap -->
<script src="/js/bootstrap.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
<script src="/revolution/js/revolution.extension.actions.min.js"></script>
<script src="/revolution/js/revolution.extension.carousel.min.js"></script>
<script src="/revolution/js/revolution.extension.kenburn.min.js"></script>
<script src="/revolution/js/revolution.extension.layeranimation.min.js"></script>
<script src="/revolution/js/revolution.extension.migration.min.js"></script>
<script src="/revolution/js/revolution.extension.navigation.min.js"></script>
<script src="/revolution/js/revolution.extension.parallax.min.js"></script>
<script src="/revolution/js/revolution.extension.slideanims.min.js"></script>
<script src="/revolution/js/revolution.extension.video.min.js"></script>
<script src="/revolution/js/revolution.initialize.js"></script>
<!-- Slider -->
<script src="/js/owl.carousel.js"></script>
<script src="/js/jquery.cycle.all.js"></script>
<!-- Jquery Ui -->
<script src="/js/jquery-ui.js"></script>
<!-- PrettyPhoto -->
<script src="/js/jquery.prettyPhoto.js"></script>
<!-- Isotops -->
<script src="/js/isotope-docs.min.js"></script>
<!-- Custom -->
<script src="/js/custom.js"></script>

<!-- Carousel -->
<script  src="/js/jquery.jcarousel.min.js"></script>
<script  src="/js/jcarousel.connected-carousels.js"></script>

<script>
    $(document).ready(function(){

        $(function(){
            var current = location.pathname;
            $('#nav li a').each(function(){
                var $this = $(this);
                // if the current path is like this link, make it active
                if($this.attr('href').indexOf(current) !== -1){
                    $this.addClass('active');
                }
            })
        })

    });
</script>

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/e9a5bf9ec1f1d9f98173c7f4e/9ba6e910978d63a12ca962451.js");</script>

@yield('js')

</body>

</html>

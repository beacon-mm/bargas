@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3 section_margin">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>Testimonials</h1>
                        <div class="beadcrumb">
                            <a href="{{ route('home') }}">Home</a> <i class="fa fa-angle-right"></i> <span>Testimonials</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->

    <!--Start Features-->
    <div class="features_wrap">
        <div class="container">
            <div id="doctible-review-widget"></div>
        </div>
    </div>
    <!--End Features-->

    @endsection

@section('js')

    <script type="text/javascript" src="https://www.doctible.com/v1/doctible.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            $('#doctible-review-widget').reviewWidget({
                id: 'bargas-wellness-edmond-ok',
                format: 'full'
            });
        });
    </script>

    @endsection

@extends('layouts.front')

@section('content')

        <!--Start Page Title-->
        <div class="page_title bg3 section_margin">
            <div class="layer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h1>Services</h1>
                            <div class="beadcrumb">
                                <a href="#">Home</a> <i class="fa fa-angle-right"></i> <span>Services</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Page Title-->

        @foreach(array_reverse($services['data']) as $service)
        <!--Start Features-->
        <a name="integrative-cardiology"></a>
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 fet_img">
                        <img src="{{ $service['image'] }}">
                    </div>
                    <div class="col-sm-12 col-md-6 detld">
                        <div class="heading">
                            <h3>{{ $service['title'] }}</h3>
                        </div>
                        {!! $service['content'] !!}
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->
        @endforeach

        <hr>


    @endsection
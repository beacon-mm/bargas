@extends('layouts.front')

@section('content')

        <!--Start Page Title-->
        <div class="page_title bg3 section_margin">
            <div class="layer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h1>Wellness</h1>
                            <div class="beadcrumb">
                                <a href="#">Home</a> <i class="fa fa-angle-right"></i> <span>wellness</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Page Title-->

        <!--Start Services-->
        <div class="services_wrap section_margin">
            <div class="heading">
                <h3>What does “wellness” mean?</h3>
                <p>
                    Wellness is defined as “the state of being in good health, especially as an actively pursued goal.”
                </p>
                <p>
                    Dr. Bargas believes that to be truly well there must be a balance of multiple factors which include:
                </p>
            </div>
        </div>
        <!--End Services-->

        <!--Start Features-->
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4 fet_img">
                        <img src="/image/wellness-optimal-nutrition.jpg">
                    </div>
                    <div class="col-sm-8 col-md-8 detld">
                        <div class="heading">
                            <h3>Optimal <span>Nutrition</span></h3>
                        </div>
                        <p>This may include supplements that are lacking in the diet of today’s world. What we put into our bodies is the number one contributor to disease development and propagation. Longstanding impact in the betterment of our health comes in the form of long-term lifestyle changes, not fad diets.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->

        <!--Start Features-->
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4 fet_img">
                        <img src="/image/wellness-hormone-balance.jpg">
                    </div>
                    <div class="col-sm-8 col-md-8 detld">
                        <div class="heading">
                            <h3>Hormone <span>Balance</span></h3>
                        </div>
                        <p>The endocrine system is very complex and largely responsible for how we feel.  Often times getting to the root of the problem necessitates digging beyond standard laboratory tests. At Bargas Wellness, we are prepared to do just that. For example, a normal TSH does not necessarily mean a normal functioning thyroid.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->

        <!--Start Features-->
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4 fet_img">
                        <img src="/image/wellness-adequate-rejuvenating-sleep.jpg">
                    </div>
                    <div class="col-sm-8 col-md-8 detld">
                        <div class="heading">
                            <h3>Adequate <span>Rejuvenating Sleep</span></h3>
                        </div>
                        <p>During sleep is when cell repair occurs. Sleep also helps control metabolism. Poor sleep contributes to depression, hormone imbalance and weight gain. Bargas Wellness wants to help you get the best night’s sleep possible.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->

        <!--Start Features-->
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4 fet_img">
                        <img src="/image/wellness-movement.jpg">
                    </div>
                    <div class="col-sm-8 col-md-8 detld">
                        <div class="heading">
                            <h3>Movement</h3>
                        </div>
                        <p>Our bodies need some form of routine physical activity. We do not have to call it exercise! It may be as little as standing up at your desk several times per day. The Wellness Room at Bargas Wellness is specifically designed to offer a space for movement and other activities to foster your wellness.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->

        <!--Start Features-->
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4 fet_img">
                        <img src="/image/wellness-mindfullness.jpg">
                    </div>
                    <div class="col-sm-8 col-md-8 detld">
                        <div class="heading">
                            <h3>Mindfullness</h3>
                        </div>
                        <p>Chronic stress has been linked to diabetes, immune disorders and peptic ulcer disease.  In contrast, studies suggest that meditation may reduce blood pressure, symptoms of irritable bowel syndrome, anxiety, depression and insomnia. Hugs decrease depression and anxiety and increase oxytocin levels which decreases blood pressure. Thus, we always recommend you be a good person and love people! The Wellness Room at Bargas Wellness is specifically designed to offer a space for meditation, mindfulness and other activities to foster your overall wellness.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->

        <hr>

    @endsection
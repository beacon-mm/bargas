@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3 section_margin">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>{{ $products->name }}</h1>
                        <div class="beadcrumb">
                            <a href="{{ URL::to('') }}">Home</a> <i class="fa fa-angle-right"></i> <span>Products</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->


    <!--Start Blog -->
    <div class="shop_wrap section_margin">
        <div class="container">
            <div class="row">

                <!--Start Sidebar-->
                {{--<div class="col-sm-12 col-md-3">--}}
                    {{--<aside>--}}
                        {{--@foreach($categories as $category)--}}
                        {{--<div class="side_widget animated slide">--}}
                            {{--<h5 class="heading_c">{{ $category->name }}</h5>--}}
                            {{--<ul class="shop_cates">--}}
                                {{--@foreach($category->children as $subcategory)--}}
                                {{--<li><a href="{{ action('PagesController@products', $subcategory->id . '-' . str_slug($subcategory->name)) }}">{{ $subcategory->name }}</a></li>--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}

                    {{--</aside>--}}
                {{--</div>--}}
                <!--End Sidebar-->

                <!--Start Articles-->
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="row">

                        @foreach($products->products as $product)

                        <div class="col-sm-4 col-md-4 col-xs-6 full-wdth">
                            <div class="shop">
                                <figure>
                                    <a href="{{ action('PagesController@product', $product->id . '-' . str_slug($product->name)) }}"><img src="{{ $product->image }}" alt="{{ $product->name }}"/></a>
                                </figure>
                                <h5><a href="{{ action('PagesController@product', $product->id . '-' . str_slug($product->name)) }}">{{ $product->name }}</a></h5>
                                {{--<span>$30.50</span>--}}
                                <a href="{{ action('PagesController@product', $product->id . '-' . str_slug($product->name)) }}" class="button borderd">details</a>
                            </div>
                        </div>

                        @endforeach

                    </div>
                </div>
                <!--End Articles-->

            </div>
        </div>
    </div>
    <!--End Blog -->

    @endsection

@section('js')

    <script>
        $(document).ready(function(){
            var links = $('a[href*="1463"]');
            links.attr('href', 'https://bargaswellness.goherbalife.com/Catalog/Home/Index/en-US/');
            links.attr('target', '_blank');
            console.log(links.length)
        });
    </script>

    @endsection
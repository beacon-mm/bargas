@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>Policies</h1>
                        <div class="beadcrumb">
                            <a href="{{ action('PagesController@home') }}">Home</a> <i class="fa fa-angle-right"></i> <span>policies</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->

    <div class="contact_wrap">
        <div class="container">

            <div class="panel panel-default">
                <div class="panel-heading">
                    RETURN / REFUND POLICY
                </div>
                <div class="panel-body">
                    <p>Only unopened products can be returned for a refund of the original product price. Products can be brought to the office or shipped at the expense of the purchaser. </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    PRIVACY POLICY
                </div>
                <div class="panel-body">
                    <p>Bargas Wellness will not distribute any personal information shared on the Bargas Wellness website.</p>
                </div>
            </div>

        </div>
    </div>

    @endsection
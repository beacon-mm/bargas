@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3 section_margin">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="Calendar-sm-12 col-md-12">
                        <h1>Calendar</h1>
                        <div class="beadcrumb">
                            <a href="{{ URL::to('') }}">Home</a> <i class="fa fa-angle-right"></i> <span>Calendar</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->

    @foreach($events->data as $event)
        <!--Start Features-->
        <a name="integrative-cardiology"></a>
        <div class="features_three">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 fet_img">
                        <img src="{{ $event->image }}">
                    </div>
                    <div class="col-sm-12 col-md-6 detld">
                        <div class="heading">
                            <h3>{{ $event->title }}</h3>
                        </div>
                        <div class="calendar-item-content">
                            {!! $event->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Features-->
    @endforeach

    <hr>


@endsection
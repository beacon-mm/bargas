@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3 section_margin">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>About Dr. Royce Bargas</h1>
                        <div class="beadcrumb">
                            <a href="{{ URL::to('') }}">Home</a> <i class="fa fa-angle-right"></i> <span>About Dr. Royce Bargas</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->

    <!--Start Features-->
    <div class="features_wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-3 feature_img">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <img src="/image/dr-bargas-img.jpg" alt="" class="img-rounded" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="heading">
                        <h3>Royce Bargas, DO</h3>
                        <p>Dr. Bargas is board certified in Internal Medicine, Cardiovascular Disease and Clinical Cardiac Electrophysiology and will soon be certified by the Institute for Functional Medicine. She has been in practice in Oklahoma City since November 2010. At Bargas Wellness, she uses her roots as an osteopath and the fundamentals of functional medicine to team up with her patients and help them get to the root cause of disease processes. She uses the evidence-based practice of assisting individuals and families to adopt and sustain behaviors that can improve health and quality of life.</p>

                        <h3>Medical Education</h3>
                        <ul class="about-list">
                            <li>
                                Doctorate of Osteopathic Medicine from Touro University College of Osteopathic Medicine in Vallejo, California
                            </li>
                            <li>
                                Osteopathic Internship at Henry Ford Hospital in Detroit, Michigan
                            </li>
                            <li>
                                Internal Medicine Residency and Fellowships in cardiovascular disease and clinical cardiac electrophysiology at the University of Colorado Health Sciences Center in Denver, Colorado
                            </li>
                            <li>
                                Certification in Functional Medicine, in process, through the Institute of Functional Medicine
                            </li>
                        </ul>

                        <h3>Professional Organizations</h3>
                        <ul class="about-list">
                            <li>
                                American Heart Association — Circle of Red
                            </li>
                            <li>
                                Oklahoma Osteopathic Association
                            </li>
                            <li>
                                Heart Rhythm Society
                            </li>
                            <li>
                                American College of Cardiology
                            </li>
                            <li>
                                Institute of Functional Medicine
                            </li>
                        </ul>

                        <h3>Personal Interests</h3>
                        <p>Dr. Bargas is an avid outdoorswoman and amateur gourmet chef who enjoys travelling with her husband, Jon, and her son, J.T. She has served as Chair of the Oklahoma City Circle of Red, an American Heart Association program dedicated to raising awareness of heart disease in women. Dr. Bargas is a member of Crossings Community Church and currently volunteers her time and expertise at Crossing Community Clinic, which provides medical care to Oklahomans who are uninsured or otherwise without access to health care. </p>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--End Features-->

    @endsection
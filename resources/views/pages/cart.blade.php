@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3 section_margin">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>Cart</h1>
                        <div class="beadcrumb">
                            <a href="{{ action('PagesController@home') }}">Home</a> <i class="fa fa-angle-right"></i> <span>Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->
    <!--start-->
    <section>
        <div class="container">
            <div class="row">
                <div class="cart_table_wraper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="cart_wrpaer">
                                <div class="table_scroll table-responsive">
                                    <table class=" table table-striped">
                                        <thead class="dark-bg">
                                        <tr>
                                            <th><span>Description</span></th>
                                            <th><span>Price</span></th>
                                            <th><span>Quantity</span></th>
                                            <th><span>Total</span></th>
                                            <th><span></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if( count(Cart::content()) < 1 )
                                        <tr>
                                            <td colspan="5" class="text-center">Your Cart is empty.</td>
                                        </tr>
                                        @endif
                                        @foreach( Cart::content() as $item )
                                        <tr>
                                            <td class="padding_all">
                                                <p>
                                                    {{ $item->name }}
                                                </p>
                                            </td>
                                            <td class="padding_all">
                                                <p>
                                                    ${{ number_format($item->price, 2) }}
                                                </p>
                                            </td>
                                            <td class="padding_all">
                                                <div class="quantity">
                                                    <form action="{{ action('CartController@update', $item->rowId) }}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
                                                        <input step="1" min="1" max="5" name="qty" value="{{ $item->qty }}" title="Qty" class="input-text qty text text-center" size="4" type="number">
                                                    </form>
                                                </div>
                                            </td>
                                            <td class="padding_all">
                                                <p>
                                                    ${{ number_format($item->subtotal, 2) }}
                                                </p>
                                            </td>
                                            <td class="padding_all"><a href="#" onclick="remove('{{ $item->rowId }}')"><i class="fa fa-times"></i></a></td>
                                            <form id="remove-{{ $item->rowId }}" action="{{ action('CartController@destroy', $item->rowId) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!-- table End -->
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <p class="lead text-right">
                                        Subtotal: {{ Cart::subtotal() }}<br />
                                        Shopping: 10.00<br />
                                        Taxes: {{ Cart::tax() }}<br />
                                        Total: {{ number_format(Cart::total() + 10, 2) }}
                                    </p>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Checkout
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="city" value="{{ old('city') }}" placeholder="City" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="zip_code" value="{{ old('zip_code') }}" placeholder="Zip Code" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required >
                                            </div>
                                            <div class="form-group">
                                        <textarea name="comments" id="comments" cols="30" rows="10"
                                                  class="form-control" placeholder="Comments">{{ old('comments') }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="credit_card_number" placeholder="Credit Card Number" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" min="3" max="3" placeholder="CVV" required >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Expiration Date: MM/YYYY"  >
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="cardholder" placeholder="Name On Card">
                                            </div>
                                        </div>
                                    </div>

                                    <button class="button">
                                        Process Order
                                    </button>
                                </div>
                            </div>

                        </div>
                        <!-- column End -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <hr>

    @endsection

@section('js')

    <script>

        $('.qty').on('change', function(e){

            e.preventDefault();

            $(this).closest('form').submit();

        });

        function remove(id)
        {
            var form = '#remove-' + id;

            $(form).submit();
        }

    </script>

    @endsection
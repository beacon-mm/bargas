@extends('layouts.front')

@section('content')

    <!--Start Page Title-->
    <div class="page_title bg3">
        <div class="layer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>Contact Us</h1>
                        <div class="beadcrumb">
                            <a href="#">Home</a> <i class="fa fa-angle-right"></i> <span>get in touch</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Page Title-->

    <div class="contact_wrap">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="heading">
                        <h3>Call us <span>now!</span></h3>
                    </div>

                    <ul>
                        <li><strong>United States:</strong> 405-607-4445</li>
                    </ul>

                </div>

                <div class="col-sm-12 col-md-7 mg-top-40">
                    <div class="heading">
                        <h3>Inquiry <span>Form</span></h3>
                    </div>
                    <p class="success alert alert-success" id="success" style="display:none;"></p>
                    <p class="error alert alert-danger" id="error" style="display:none;"></p>
                    <form name="contact_form_3" id="contact_form_3" method="post" action="{{ action('FormsController@contact_me') }}" >
                        {{ csrf_field() }}
                        <div class="row">
                            <fieldset class="col-sm-6 col-md-6">
                                <input type="text" name="first_name" value="{{ old('name') }}" placeholder="First Name"/>
                            </fieldset>
                            <fieldset class="col-sm-6 col-md-6">
                                <input type="text" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name"/>
                            </fieldset>
                            <fieldset class="col-sm-6 col-md-6">
                                <input type="text" name="email" value="{{ old('email') }}" placeholder="Email"/>
                            </fieldset>
                            <fieldset class="col-sm-6 col-md-6">
                                <input type="text" name="phone" value="{{ old('phone') }}" placeholder="Phone"/>
                            </fieldset>
                            <div class="col-sm-12 col-md-12">
                                <textarea name="comments" placeholder="Message">{{ old('comments') }}</textarea>
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" name="form" value="Contact Me">
                            <div class="form-group">
                                {!! Recaptcha::render() !!}
                            </div>
                            <button type="submit" id="submit_3" class="button pd-left-15"/>Submit</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

    @endsection
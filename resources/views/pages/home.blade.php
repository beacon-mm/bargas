@extends('layouts.front')

@section('content')

    <div class="home-video section-margin visible-xs">
        <div class="container">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/319241714"></iframe>
            </div>
        </div>
    </div>

    <!--Start Slider-->
    <section class="main-slider indxone hidden-xs" id="home">
        <div class="container">
            <div id="rev_slider_476_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="christmas-snow-scene" data-source="gallery" style="background-color:transparent;padding:0px;">
                <!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
                <div id="rev_slider_476_1" class="rev_slider" style="display:none;" data-version="5.3.0.2">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-1648" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="images/slider_banner.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="/image/slide1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lyr2 tp-resizeme" id="slide-1648-layer-2" data-x="left" data-hoffset="20" data-y="center" data-voffset="15" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1700,"to":"o:1;","delay":1000,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]' data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[15,15,15,15]" data-paddingleft="[0,0,0,0]" data-start="2000" data-splitin="chars" data-splitout="none" data-elementdelay="0.06" style="">
                                Mission Statement
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lyr3 tp-resizeme" id="slide-1648-layer-3" data-x="left" data-hoffset="20" data-y="center" data-voffset="60" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]' data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-start="1400" data-splitin="" data-splitout="none" data-elementdelay="0.02" style="">
                                Our goal at Bargas Wellness is to help people function at their highest potential and feel their absolute best.
                            </div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-1649" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="images/slider_banner.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="/image/slide2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lyr2 tc tp-resizeme" id="slide-1648-layer-6" data-x="center" data-hoffset="0" data-y="center" data-voffset="-25" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1700,"to":"o:1;","delay":1000,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]' data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[15,15,15,15]" data-paddingleft="[0,0,0,0]" data-start="2000" data-splitin="chars" data-splitout="none" data-elementdelay="0.06" style="">
                                How Bargas Wellness is Different
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lyr3 tc tp-resizeme" id="slide-1648-layer-7" data-x="center" data-hoffset="0" data-y="center" data-voffset="60" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]' data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-start="1400" data-splitin="" data-splitout="none" data-elementdelay="0.02" style="">
                                Dr. Bargas is board certified in Internal Medicine,
                                <br />Cardiovascular Disease and Clinical Cardiac Electrophysiology and will soon be certified by the Institute for Functional Medicine.
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Revolution Slider end-->
        </div>
    </section>
    <!--main-slider end-->
    <!--End Slider-->
    <!--Start Main Info-->
    <div class="main_info section_margin" id="appointment">
        <div class="container">
            <div class="info_inner">
                <div class="row">
                    <div class="col-sm-1 col-md-1"></div>
                    <div class="col-sm-5 col-md-5">
                        <h4>opening hours</h4>
                        <span>Monday - Friday : 8:00AM to 4:30PM</span>
                        <strong>Call Us Anytime</strong>
                        <h5><i class="fa fa-phone"></i> 405-607-4445</h5>
                        <hr>
                        <h1 class="text-center">Already a patient?</h1>
                        <p class="text-center">
                            <a class="btn btn-primary" target="_blank" href="https://mycw124.ecwcloud.com/portal17202/jsp/100mp/login_otp.jsp">
                                Click here to access your Online Portal
                            </a>
                        </p>
                    </div>
                    <a name="request-appointment"></a>
                    <div class="col-sm-5 col-md-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h4>Appointment Request</h4>
                            </div>
                            <form action="{{ action('FormsController@contact_me') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="form" value="Appointment Request" >
                                <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" required >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone') }}" required >
                                </div>
                                <div class="form-group">
                                    <textarea name="comments" id="comments" cols="30" rows="5"
                                              class="form-control" placeholder="Comments" required >{{ old('comments') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" name="date" placeholder="Date" value="{{ old('date') }}" required >
                                </div>
                                <div class="form-group">
                                    <div class="width: 303px; margin: 0 auto;">
                                        {!! Recaptcha::render() !!}
                                    </div>
                                </div>
                                <input class="button" type="submit" value="Request My Appointment" />
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-1 col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
    <!--End Main Info-->

    <!--Start Services-->
    <div class="services_wrap section_margin">
        <div class="heading">
            <h3>Mission Statement</h3>
            <p>
                Our goal at Bargas Wellness is to help people function at their highest potential and feel their absolute best.<br />
                We hope in doing that, we help people live longer, more fulfilled lives.<br />
                We accomplish this through education and empowering the patient to be the leader of their therapeutic team.
            </p>
        </div>
    </div>
    <!--End Services-->

    <div class="home-video section-margin hidden-xs">
        <div class="container">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tvvJmFf6flw"></iframe>
            </div>
            <hr>
            <div class="lead text-center">
                <a href="https://www.youtube.com/channel/UCQfu2a9aw6LtGP89x7XdV3Q" target="_blank" >
                    <i class="fa fa-play"></i> To view all of Dr. Bargas’s videos click here
                </a>
            </div>
        </div>
    </div>

    <!--Start Statics-->
    <div class="stat_wrap section_margin">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="stat">
                        <img src="image/stat_3.png" alt="" />
                        <h3>More Than 10 Years of Experience</h3>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="stat">
                        <img src="image/stat_1.png" alt="" />
                        <h3>Oklahoma’s Premier Integrative Cardiologist</h3>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="stat">
                        <img src="image/stat_2.png" alt="" />
                        <h3>Serving Patients throughout All of Oklahoma</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Start Statics-->

    <!--Start Departments-->
    <div class="department section_margin">
        <div class="inner_dep">
            <div class="container">
                <div class="row">
                    <div class="heading">
                        <h3>{{ $block_how->title }}</h3>
                        {!! $block_how->content !!}
                        <h3>{{ $block_what->title }}</h3>
                        {!! $block_what->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Departments-->

    <!--Start Services-->
    <div class="services_wrap services_two section_margin">
        <div class="container">
            <div class="row">
                <div class="heading">
                    <h3>Services</h3>
                </div>
                <div class="col-sm-6 col-md-6">
                    <a href="{{ action('PagesController@services') }}#integrative-cardiology">
                        <div class="service" style="background: url(/image/home-integrative-cardiology.jpg); background-size: cover;">
                            <h5>Integrative Cardiology</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6">
                    <a href="{{ action('PagesController@services') }}#functional-medicine">
                        <div class="service" style="background: url(/image/home-functional-medicine-replaced.jpg); background-size: cover;">
                            <h5>Functional Medicine</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6">
                    <a href="{{ action('PagesController@services') }}#hormone-balance">
                        <div class="service" style="background: url(/image/home-hormone-balance.jpg); background-size: cover;">
                            <h5>Hormone Balance</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-6">
                    <a href="{{ action('PagesController@services') }}#medical-weight-loss">
                        <div class="service" style="background: url(/image/home-medical-weight-loss.jpg); background-size: cover;">
                            <h5>Medical Weight Loss</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--End Services-->

    @endsection
